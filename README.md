------------------------------------------------------------------------------

                             OPENAIR-CN-5G
 An implementation of the 5G Core network by the OpenAirInterface community.

------------------------------------------------------------------------------

OPENAIR-CN-5G is an implementation of the 3GPP specifications for the 5G Core Network.
At the moment, it contains the following network elements:

* Access and Mobility Management Function (**AMF**)
* Authentication Server Management Function (**AUSF**)
* Network Repository Function (**NRF**)
* Session Management Function (**SMF**)
* Unified Data Management (**UDM**)
* Unified Data Repository (**UDR**)
* User Plane Function (**UPF**)
* Network Slicing Selection Function (**NSSF**)

Each has its own repository: this repository (`oai-cn5g-upf-sdfabric`) is meant for UPF.

This `UPF` repository contains mainly scripts and configuration files to run and test SD-Fabric UPF with OAI core network.<br/><br/>
[SD-Fabric](https://docs.sd-fabric.org/master/index.html) is an open source, full stack, deeply programmable network fabric optimized for edge cloud, 5G, and Industry 4.0 applications. It builds on SDN and cloud native principles to create a disruptive platform that for the first time empowers the network with modern cloud development principles.

# Licence info

SD-Fabric has following components
* [PFCP Agent](https://github.com/omec-project/upf) - It is distributed under Apache License
* [UP4 APP](https://github.com/omec-project/up4) - It is distributed under Apache License
* [ONOS SDN Controller](https://github.com/opennetworkinglab/onos.git) - It is distributed under Apache License

# Where to start

Refer [wiki page](https://gitlab.eurecom.fr/oai/cn5g/oai-cn5g-upf-sdfabric/-/wikis/Deployment-using-Docker) for instructions related to test SD-Fabric UPF with OAI Core Network. <br/>

For more details on the supported feature please check corresponding official project site.

# Collaborative work

This source code is managed through a GITLAB server, a collaborative development platform:

*  URL: [https://gitlab.eurecom.fr/oai/cn5g/oai-cn5g-upf-sdfabric.git](https://gitlab.eurecom.fr/oai/cn5g/oai-cn5g-upf-sdfabric.git).

Process is explained in [CONTRIBUTING](CONTRIBUTING.md) file.

# Contribution requests

In a general way, anybody who is willing can contribute on any part of the
code in any network component.

Contributions can be simple bugfixes, advices and remarks on the design,
architecture, coding/implementation.
